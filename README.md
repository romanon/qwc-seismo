SEISMO | QGIS Web Client 2 (QWC2)
==================================

QGIS Web Client 2 (QWC2) is a modular next generation responsive web client for QGIS Server, built with ReactJS and OpenLayers. This repository contains the SEISMO configuration of QWC2. 

## Requirements
* [QGIS Desktop](https://www.qgis.org/en/site/forusers/download.html) to configure the project and generate the QGIS server project file 
* [QGIS server](https://docs.qgis.org/3.34/en/docs/training_manual/qgis_server/install.html) to serve the QGIS project file
* [Node.js](https://nodejs.org/en/download/) to run the development server and build the production files
* [Yarn](https://classic.yarnpkg.com/en/docs/install) to install the dependencies and run the development server


## Documentation

* [QWC2](https://qwc-services.github.io/master/)
* [Internal configuration](https://wiki.seismo.ethz.ch/doku.php?id=start)
  

## Installation

```bash
yarn install
```
## Development

```bash
yarn start
```

# License

QWC2 is released under the terms of the [BSD license](https://github.com/qgis/qwc2-demo-app/blob/master/LICENSE).
